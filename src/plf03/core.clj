(ns plf03.core)

;COMP

(defn función-comp-1
  []
  (let [f (fn [x] (+ 13 x))
        g (fn [y] (- 4 y))
        z (comp f g)]
    (z 7)))

(defn función-comp-2
  []
  (let [f (fn [x] (* 8 x))
        g (fn [y] (+ y 2) )
        z (comp f g)]
    (z 10)))

(defn función-comp-3
  []
  (let [f (fn [x] (str x 2))
        g (fn [s] (str s 1))
        z (comp f g)]
         (z "gg")))

(defn función-comp-4
  []
  (let [f (fn [x] (concat x [1 2 3 4]))
        g (fn [xs] (concat xs [38 12 04]))
        z (comp f g)]
    (z [8 63 1])))

(defn función-comp-5
  []
  (let [f (fn [x] (take  2 x ))
        g (fn [xs] (take 3 xs ))
        z (comp f g)]
    (z [8 63 1 1 4 5 68 9162 412 536 12])))

(defn función-comp-6
  []
  (let [f (fn [x] (inc x))
        g (fn [y] (- 5 y))
        z (comp f g)]
    (z 7)))

(defn función-comp-7
  []
  (let [f (fn [x] (filter pos? x))
        g (fn [xs] (map inc xs))
        z (comp f g)]
    (z #{-20 10 -3 5 -4 8 4 7 -10 12})))

(defn función-comp-8
  []
  (let [f (fn [x] (keyword x))
        g (fn [xs] (str xs))
        z (comp f g)]
    (z ["a" "b0" "c1"])))

(defn función-comp-9
  []
  (let [f (fn [x] (- 3 x))
        g (fn [y] (* 2 y))
        z (comp f g)]
    (z 24)))

(defn función-comp-10
  []
  (let [f (fn [x] (* 8 x))
        g (fn [y] (+ 22 y))
        z (comp f g)]
    (z 30)))


(defn función-comp-11
  []
  (let [f (fn [x] (- x 23))
        g (fn [y] (* 89 y))
        z (comp f g)]
    (z 100)))

(defn función-comp-12
  []
  (let [f (fn [x] (- 3 x))
        g (fn [y] (- 22 y))
        z (comp f g)]
    (z 30)))

(defn función-comp-13
  []
  (let [f (fn [x] (* x 2))
        g (fn [y] (* y 3))
        z (comp f g)]
    (z 10)))

(defn función-comp-14
  []
  (let [f (fn [x] (not x))
        g (fn [y] (zero? y))
        z (comp f g)]
    (z 5)))


(defn función-comp-15
  []
  (let [f (fn [x] (boolean? x))
        g (fn [xs] (last xs))
        z (comp f g)]
    (z [true 1243 ])))

(defn función-comp-16
  []
  (let [f (fn [x] (indexed? x))
        g (fn [xs] (vector xs))
        z (comp f g)]
    (z [1 2 43 455 6])))


(defn función-comp-17
  []
  (let [f (fn [x] (list? x))
        g (fn [xs] (map-entry? xs))
        z (comp f g)]
    (z [9 4 12 3 4])))

(defn función-comp-18
  []
  (let [f (fn [x] (str x))
        g (fn [y] (double y))
        z (comp f g)]
    (z 976)))


(defn función-comp-19
  []
  (let [f (fn [x] (+ 85 x))
        g (fn [y] (- 223 y))
        z (comp f g)]
    (z 23)))

(defn función-comp-20
  []
  (let [f (fn [x] (char? x))
        g (fn [s] (first s))
        z (comp f g)]
    (z "asdzxcaq")))


(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)

;COMPLEMENT

(defn función-complement-1
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z true)))

(defn función-complement-2
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 20)))

(defn función-complement-3
  []
  (let [f (fn [x] (pos? x))
        z (complement f)]
    (z 15)))

(defn función-complement-4
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 25)))

(defn función-complement-5
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 53)))

(defn función-complement-6
  []
  (let [f (fn [x] (associative? x))
        z (complement f)]
    (z [1 3 5 7])))

(defn función-complement-7
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z \e)))

(defn función-complement-8
  []
  (let [f (fn [x] (keyword? x))
        z (complement f)]
    (z :hola)))

(defn función-complement-9
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 23)))

(defn función-complement-10
  []
  (let [f (fn [x] (string? x))
        z (complement f)]
    (z "cadena01")))

(defn función-complement-11
  []
  (let [f (fn [x] (map? x))
        z (complement f)]
    (z {:a 1 :b 2 :c 3})))

(defn función-complement-12
  []
  (let [f (fn [x] (nat-int? x))
        z (complement f)]
    (z (+ 10 1))))

(defn función-complement-13
  []
  (let [f (fn [x] (map? x))
        z (complement f)]
    (z (first [1 2 2]))))

(defn función-complement-14
  []
  (let [f (fn [x] (coll? x))
        z (complement f)]
    (z  [1 4 34 53 6])))

(defn función-complement-15
  []
  (let [f (fn [x] (rational? x))
        z (complement f)]
    (z 3/5)))

(defn función-complement-16
  []
  (let [f (fn [xs] (map even? xs))
        z (complement f)]
    (z {:q 1 :a 2 :g 3})))

(defn función-complement-17
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z true)))

(defn función-complement-18
  []
  (let [f (fn [x] (number? x))
        z (complement f)]
    (z 23)))

(defn función-complement-19
  []
  (let [f (fn [x] (number? x))
        z (complement f)]
    (z (last [59 73 123 743]))))

(defn función-complement-20
  []
  (let [f (fn [x] (neg-int? x))
        z (complement f)]
    (z 10M)))


(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

;CONSTANTLY
(defn función-constantly-1
  []
  (let [x true
        z (constantly x)]
    (z 1)))

(defn función-constantly-2
  []
  (let [x "Constante"
        z (constantly x)]
    (z [1 3894 4])))

(defn función-constantly-3
  []
  (let [x 123
        z (constantly x)]
    (z  {:hola "Hola" :mundo "Mundo"})))

(defn función-constantly-4
  []
  (let [x \U
        z (constantly x)]
    (z  {:Assignature "PLF" :horario "5-6"})))

(defn función-constantly-5
  []
  (let [x \U
        z (constantly x)]
    (z  {:Name "Fer"})))

(defn función-constantly-6
  []
  (let [x [:a "Buenas " :b "Noches"]
        z (constantly x)]
    (z  #{1 2 3 4 68 612})))

(defn función-constantly-7
  []
  (let [x (take 3 (iterate inc 10))
        z (constantly x)]
    (z  #{1 2 3 4 68 612})))

(defn función-constantly-8
  []
  (let [x (take 3 (iterate dec 10))
        z (constantly x)]
    (z  #{87 612 5 624})))

(defn función-constantly-9
  []
  (let [x (char? \s)
        z (constantly x)]
    (z  (take 5 (iterate inc 20)))))

(defn función-constantly-10
  []
  (let [x (coll? [1 2 3 4])
        z (constantly x)]
    (z  20)))

(defn función-constantly-11
  []
  (let [x 1.9M
        z (constantly x)]
    (z  decimal? 20)))

(defn función-constantly-12
  []
  (let [x 0.873
        z (constantly x)]
    (z  float? 40)))

(defn función-constantly-13
  []
  (let [x "Integer"
        z (constantly x)]
    (z  int? 54)))

(defn función-constantly-14
  []
  (let [x [:a \a :b \b :c \c :e \e]
        z (constantly x)]
    (z  int 54M)))

(defn función-constantly-15
  []
  (let [x  1231245125412512
        z (constantly x)]
    (z  rational? 200)))

(defn función-constantly-16
  []
  (let [x  "Constantly 16"
        z (constantly x)]
    (z  rational? [\a "g"])))

(defn función-constantly-17
  []
  (let [x  false
        z (constantly x)]
    (z  [2 3 54 5])))

(defn función-constantly-18
  []
  (let [x  [true true true]
        z (constantly x)]
    (z  1 2 3)))

(defn función-constantly-19
  []
  (let [x  [true false false true]
        z (constantly x)]
    (z boolean?  true 1 2 false)))

(defn función-constantly-20
  []
  (let [x  25
        z (constantly x)]
    (z * 4 5)))


(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

;every-pred

 (defn función-every-pred-1
   []
   (let [f (fn [x] (number? x))
         g (fn [y] (pos? y))
         z (every-pred f g)]
     (z 212)))

(defn función-every-pred-2
  []
  (let [f (fn [xs] (count xs))
        g (fn [ys] filter pos? ys)
        z (every-pred f g)]
    (z [1 2 3 4])))

(defn función-every-pred-3
  []
  (let [f (fn [x] (take 3 (iterate dec x)))
        g (fn [n] float n)
        z (every-pred f g)]
    (z 20)))

(defn función-every-pred-4
  []
  (let [f (fn [x] (boolean? x))
        g(fn [s] (string? s))
        z (every-pred f g)]
    (z "hola")))

(defn función-every-pred-5
  []
  (let [f (fn [x] (filter int? x))
        g (fn [xs] (filter neg? xs))
        z (every-pred f g)]
    (z [9 3 -2.3 -4.23 -3 -4 -55])))


(defn función-every-pred-6
  []
  (let [f (fn [x] (filter ratio? x))
        g (fn [xs] (filter ident? xs))
        z (every-pred f g)]
    (z [ 9 73 52 173 1 4 5 134 ])))

(defn función-every-pred-7
  []
  (let [f (fn [xs] (filter double? xs))
        g (fn [ys] (filter integer? ys))
        z (every-pred f g)]
    (z '(1.9 836 316  1367))))

(defn función-every-pred-8
  []
  (let [f (fn [xs] (filter neg-int? xs))
        g (fn [ys] (filter odd? ys))
        z (every-pred f g)]
    (z '(-8 -1 2 -43))))

(defn función-every-pred-9
  []
  (let [f (fn [x] (* 5M x))
        g (fn [y] (/ 43 y))
        z (every-pred f g)]
    (z 34)))

(defn función-every-pred-10
  []
  (let [f (fn [xs] (filter odd? xs))
        g (fn [ys] (count ys))
        z (every-pred f g)]
    (z [1 2 3 4 5 6 7 7 8  9 9 0])))

(defn función-every-pred-11
  []
  (let [f (fn [x] (filter pos? x))
        g (fn [xs] (map inc xs))
        z (every-pred f g)]
    (z #{-20 10 -3 5 -4 8 4 7 -10 12})))

(defn función-every-pred-12
  []
  (let [f (fn [s] (boolean? s))
        g (fn [xs] (vector? xs))
        z (every-pred f g)]
    (z "G")))

(defn función-every-pred-13
  []
  (let [f (fn [s] (float? s))
        g (fn [ss] (char? ss))
        z (every-pred f g)]
    (z "CADENA")))

(defn función-every-pred-14
  []
  (let [f (fn [s] (double? s))
        g (fn [ss] boolean? ss)
        z (every-pred f g)]
    (z "MICHEL")))

(defn función-every-pred-15
  []
  (let [f (fn [xs] (group-by pos? xs))
        g (fn [ys] (filter odd? ys))
        z (every-pred f g)]
    (z [1 -1 2 -2 3 -3 4 -4])))

(defn función-every-pred-16
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (double? y))
        z (every-pred f g)]
    (z 98.547)))

(defn función-every-pred-17
  []
  (let [f (fn [xs] (filter double? xs))
        g (fn [ys] (char? ys))
        z (every-pred f g)]
    (z #{\a \b \s})))

(defn función-every-pred-18
  []
  (let [f (fn [x] (vector? x))
        g (fn [y] (map? y))
        z (every-pred f g)]
    (z 220)))

  (defn función-every-pred-19
    []
    (let [f (fn [x] (true? x))
          g (fn [y] (boolean? y))
          z (every-pred f g)]
      (z true)))

(defn función-every-pred-20
  []
  (let [f (fn [xs] (filter number? xs))
        g (fn [ys] filter char? ys)
        z (every-pred f g)]
    (z ["a" \a "b" \b "xc" \c])))



(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)


;fnil

(defn función-fnil-1
  []
  (let [f (fn [x] (str "Hola " x))
        y (fnil f "mundo")
        z (fnil f "a todos")]
    [(y "Gatito") (z "Perrito")]))

(defn función-fnil-2
  []
  (let [f (fn  [s] (str "Bienvenido: " s ))
        z (fnil f "")]
    (z "Fernando")))


(defn función-fnil-3
  []
  (let [f (fn [x] (map true? x))
        z (fnil f (map true? [false true true true ]))]
    (z ["false" true false false true nil])))


(defn función-fnil-4
  []
  (let [f (fn  [x s] (str "Fernando " x "+" s))
        z (fnil f nil)]
    (z nil " Sanchez")))

(defn función-fnil-5
  []
  (let [f (fn [x] (map boolean? x))
        z (fnil f "string")]
    (z #{false true})))

(defn función-fnil-6
  []
  (let [f (fn  [x] (* 6 x))
        z (fnil f nil)]
    (z 8.8)))

(defn función-fnil-7
  []
  (let [f (fn [x] (str "Welcome to the " x))
        z (fnil f "rosa")]
    (z "jungle")))

(defn función-fnil-8
  []
  (let [f (fn [x] (reverse x))
        z (fnil f (+ 2 3))]
    (z "aloh")))

(defn función-fnil-9
  []
  (let [f (fn [x] inc x)
        z (fnil f nil)]
    (z 87)))

(defn función-fnil-10
  []
  (let [f (fn  [x] (+ 18 x))
        z (fnil f nil)]
    (z 20)))


(defn función-fnil-11
  []
  (let [f (fn  [x] (/ 10 x))
        z (fnil f nil)]
    (z 65)))


(defn función-fnil-12
  []
  (let [f (fn  [s] (str "Flo" s))
        z (fnil f "")]
    (z "rida")))

(defn función-fnil-13
  []
  (let [f (fn  [s] (str "nR" s))
        z (fnil f "")]
    (z "EPL")))

(defn función-fnil-14
  []
  (let [f (fn  [x] (inc x))
        z (fnil f nil)]
    (z 5)))

(defn función-fnil-15
  []
  (let [f (fn [x] (inc x))
        z (fnil f nil)]
    (z 18)))

(defn función-fnil-16
  []
  (let [f (fn [s] (str "Cadena1 " s))
        z (fnil f "")]
    (z "Cadena2 ")))

(defn función-fnil-17
  []
  (let [f (fn [x] (inc x))
        z (fnil f nil)]
    (z 12)))

(defn función-fnil-18
  []
  (let [f (fn [x] (pos? x))
        z (fnil f nil)]
    (z 123)))

(defn función-fnil-19
  []
  (let [f (fn [x] (neg-int? x))
        z (fnil f nil)]
    (z -1209)))

(defn función-fnil-20
  []
  (let [f (fn [x] (int? x))
        z (fnil f nil)]
    (z 0.0863)))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)

;Juxt

(defn función-juxt-1
  []
  (let [f (fn [x] (+ 3 x))
        g (fn [y] (* 1 y))
        h (fn [n] (dec  n))
        z (juxt h g f)]
    (z 10)))

(defn función-juxt-2
  []
  (let [f (fn [x] (boolean? x))
        g (fn [y] (/ 2 y))
        h (fn [n] (range n))
        z (juxt f h g)]
    (z 3)))


(defn función-juxt-3
  []
  (let [f (fn [x] (filter pos? x))
        g (fn [y] (count y))
        z (juxt f g)]
    (z [1 2 3 4 5 6 7 8])))

(defn función-juxt-4
  []
  (let [f (fn [xs] (filter odd? xs))
        g (fn [ys] (count ys))
        z (juxt f g)]
    (z [1 2 3 4 5 6 7 8 9 10])))

(defn función-juxt-5
  []
  (let [f (fn [x] (first x))
        g (fn [s] (count s))
        z (juxt f g)]
    (z "Fernando")))


(defn función-juxt-6
  []
  (let [f (fn [xs] (count xs))
        g (fn [ys] (:b  ys))
        z (juxt f g)]
    (z {:a 512 :b "hola" :c 612 :d 8612})))

(defn función-juxt-7
  []
  (let [f (fn [x] (sort x))
        g (fn [s] (conj ["vector" 2] s))
        z (juxt f g)]
    (z "hola")))

(defn función-juxt-8
  []
  (let [f (fn [x y n] (max x y n))
        g (fn [xx yx xn] (min xx yx xn))
        z (juxt f g)]
    (z  98 67 12 )))

(defn función-juxt-9
  []
  (let [f (fn [xs] (remove false? xs))
        g (fn [ys] (last ys))
        z (juxt g f)]
    (z [false false 1 2 4 5 6 7 ])))

(defn función-juxt-10
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [ys] (list? ys))
        z (juxt g f )]
    (z '("juan" "pepe" "josue" "karla"))))

(defn función-juxt-11
  []
  (let [f (fn [xs] (map? xs))
        z (juxt f)]
    (z ["juan" "pepe" "josue" "karla"])))

(defn función-juxt-12
  []
  (let [f (fn [xs] (update xs 3 inc))
        g (fn [ys] (count ys))
        z (juxt g f)]
    (z [62 98 012 421 283])))

(defn función-juxt-13
  []
  (let [f (fn [xs] (drop-last xs))
        g (fn [ys] (remove pos? ys))
        z (juxt g f)]
    (z [-12 -3 4 -1 -3 4 -56 -12 ])))

(defn función-juxt-14
  []
  (let [f (fn [xs] (sort-by count xs))
        g (fn [ys] (string? ys))
        z (juxt f g)]
    (z '("gol" "hola" "gato"))))

(defn función-juxt-15
  []
  (let [f (fn [x y] (+ x y))
        g (fn [xx xy] (- xx xy))
        z (juxt g f )]
    (z 13 145)))

(defn función-juxt-16
  []
  (let [f (fn [xs] (take 2 xs))
        g (fn [ys] (drop 6 ys))
        z (juxt f g)]
    (z [5 48 584 5267 16])))

(defn función-juxt-17
  []
  (let [f (fn [xs] (count xs))
        g (fn [ys] (first ys))
        z (juxt g f)]
    (z [8.02 4 12 0.005])))

(defn función-juxt-18
  []
  (let [f (fn [xs] (filter char? xs))
        g (fn [ys] (count ys))
        z (juxt g f)]
    (z '(\a "asd" \f 24 "asd" \s \p))))


(defn función-juxt-19
  []
  (let [f (fn [xs] (first xs))
        g (fn [ys] (vector ys))
        z (juxt  g f)]
    (z [654 321 987 3214 688731 915 620 74])))


(defn función-juxt-20
  []
  (let [f (fn [xs] (map inc xs))
        g (fn [ys] (filter odd? ys))
        z (juxt g f)]
    (z #{7 8 9 1 58 23 4})))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)

;Partial

(defn función-partial-1
  []
  (let [f (fn [x y n] (vector x y n))
        z (partial f)]
    (z 4 5 6)))


(defn función-partial-2
  []
  (let [f (fn [x y n] (hash-set x y n))
        z (partial f)]
    (z 1 2 3)))

(defn función-partial-3
  []
  (let [f (fn [w x y n] (hash-map w x y n))
        z (partial f)]
    (z 123 591321 897 621897 )))

(defn función-partial-4
  []
  (let [f (fn [x y n] (vector x y n))
        z (partial f)]
    (z 346 237 1239)))

(defn función-partial-5
  []
  (let [f (fn [x y n] (list x y n))
        z (partial f)]
    (z 346 237 1239)))


(defn función-partial-6
  []
  (let [f (fn [s] (take-while char? s))
        z (partial f)]
    (z "SANCHEZ")))

(defn función-partial-7
  []
  (let [f (fn [s] (sort-by char? s))
        z (partial f)]
    (z "XIAOMI")))

(defn función-partial-8
  []
  (let [f (fn [xs] (filterv even? xs))
        z (partial f)]
    (z [100 110 120 130 111 123])))

(defn función-partial-9
  []
  (let [f (fn [xs] (sequential? xs))
        z (partial f)]
    (z [1 2 3 4 5])))

(defn función-partial-10
  []
  (let [f (fn [xs] (take-while boolean? xs))
        z (partial f)]
    (z [false true  true true 1 2 3])))


(defn función-partial-11
  []
  (let [f (fn [xs] (filter pos? xs))
        z (partial f)]
    (z [-1 -2 -3 -4 1 2 3 4 5])))

(defn función-partial-12
  []
  (let [f (fn [xs] (filter float? xs))
        z (partial f)]
    (z [1.0 2.0 3.0 1 2 3])))

(defn función-partial-13
  []
  (let [f (fn [xs] (filter integer? xs))
        z (partial f)]
    (z [1.0M 2.0M 3.0M 1 2 3])))

(defn función-partial-14
  []
  (let [f (fn [xs] (take-while number? xs))
        z (partial f)]
    (z '(1 2 3 4 5 true false true 1.2 3.2 0.122))))


(defn función-partial-15
  []
  (let [f (fn [xs] (remove char? xs))
        z (partial f)]
    (z [\a \v \a 3 4 2 1 ])))


(defn función-partial-16
  []
  (let [f (fn [xs] (filter zero? xs))
        z (partial f)]
    (z [-10 20 -30 40 0 0 0])))

(defn función-partial-17
  []
  (let [f (fn [xs] (take-while string? xs))
        z (partial f)]
    (z ["Bienvenido "  "asd" "Fernando" 123])))


(defn función-partial-18
  []
  (let [f (fn [xs] (remove float? xs))
        z (partial f)]
    (z [1.0 2.0 3.0 "Cadena"])))

(defn función-partial-19
  []
  (let [f (fn [x] (- 80 x))
        z (partial f)]
    (z 33)))

(defn función-partial-20
  []
  (let [f (fn [xs] (* 57 xs))
        z (partial f)]
    (z 23)))


(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10) 
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)
;Some-fn

(defn función-some-fn-1
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (even? y))
        h (fn [n] (double? n))
        z (some-fn f g h)]
    (z -20)))

(defn función-some-fn-2
  []
  (let [f (fn [x] (map? x))
        g (fn [y] (pos? y))
        h (fn [y] (vector? y))
        z (some-fn f g h)]
    (z 78215)))

(defn función-some-fn-3
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [ys] (map? ys))
        z (some-fn g f)]
    (z ["hola" false true 1 \a true])))

(defn función-some-fn-4
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (even? y))
        h (fn [y] (decimal? y))
        z (some-fn f g h)]
    (z -36)))

(defn función-some-fn-5
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (odd? y))
        h (fn [y] (decimal? y))
        z (some-fn f g h)]
    (z -46)))

(defn función-some-fn-6
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (double? y))
        z (some-fn f g)]
    (z 33)))

(defn función-some-fn-7
  []
  (let [f (fn [x] (map? x))
        g (fn [y] (vector? y))
        z (some-fn f g)]
    (z [12 3 4 4])))

(defn función-some-fn-8
  []
  (let [f (fn [xs] (map? xs))
        g (fn [ys] (vector? ys))
        z (some-fn f g)]
    (z [-1 -3 -4 5 12 3 4 4])))

(defn función-some-fn-9
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z 485)))

(defn función-some-fn-10
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z 485)))

(defn función-some-fn-11
  []
  (let [f (fn [x] (string? x))
        g (fn [y] (char? y))
        z (some-fn f g)]
    (z "Noche")))

(defn función-some-fn-12
  []
  (let [f (fn [xs] (map? xs))
        g (fn [ys] (vector? ys))
        z (some-fn f g)]
    (z [1 2 3 4 5])))

(defn función-some-fn-13
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (odd? y))
        h (fn [y] (int? y))
        z (some-fn f g h)]
    (z 87)))


(defn función-some-fn-14
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (even? y))
        h (fn [y] (double? y))
        z (some-fn f g h)]
    (z 462)))

(defn función-some-fn-15
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (decimal? y))
        z (some-fn f g)]
    (z 223)))


(defn función-some-fn-16
  []
  (let [f (fn [x] (odd? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z 18)))

(defn función-some-fn-17
  []
  (let [f (fn [x] (map? x))
        g (fn [y] (vector? y))
        z (some-fn f g)]
    (z {:a 2 :s 2 :d 3})))

(defn función-some-fn-18
  []
  (let [f (fn [x] (string? x))
        g (fn [y] (char? y))
        z (some-fn f g)]
    (z "String")))


(defn función-some-fn-19
  []
  (let [f (fn [x] (int? x))
        g (fn [y] (pos? y))
        z (some-fn f g)]
    (z 123456789)))

(defn función-some-fn-20
  []
  (let [f (fn [x] (odd? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z 123 )))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)
